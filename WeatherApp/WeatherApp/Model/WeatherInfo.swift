//
//  WeatherInfo.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/6/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

struct WeatherInfo: Codable {
  var latitude: Double!
  var longitude: Double!
  var timezone: String!
  var currently: TimePeriodInfo!
  var hourly: TimePeriodInfo!
  var daily: TimePeriodInfo!
  var error: String?
  var errorCode: Int?
  
  private enum CodingKeys: String, CodingKey {
    case latitude
    case longitude
    case timezone
    case currently
    case hourly
    case daily
    case error
    case errorCode = "code"
  }
}

