//
//  TimePeriodInfo.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/8/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

struct TimePeriodInfo: Codable {
  
  var time: Double!
  var windSpeed: Double!
  var summary: String!
  var temperature: Double!
  var data: [TimePeriodInfo]?
  
  private enum CodingKeys: String, CodingKey {
    case time
    case windSpeed
    case temperature
    case summary
    case data
  }
  
}
