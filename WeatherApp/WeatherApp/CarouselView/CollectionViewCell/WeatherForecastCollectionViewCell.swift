//
//  WeatherForecastCollectionViewCell.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/9/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import UIKit
import ScalingCarousel

final class WeatherForecastCollectionViewCell: ScalingCarouselCell, NibLoadableView {
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var weatherInfoLabel: UILabel!
  
  // MARK: - Public method
  
  func configure(_ timePeriod: TimePeriodInfo) {
    dateLabel.text = Date(timeIntervalSince1970: timePeriod.time).description
    weatherInfoLabel.text = timePeriod.summary
  }
}
