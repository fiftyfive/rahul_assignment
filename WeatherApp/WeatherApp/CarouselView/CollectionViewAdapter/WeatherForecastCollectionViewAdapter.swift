//
//  WeatherForecastCollectionViewAdapter.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/9/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import UIKit
import ScalingCarousel

protocol WeatherForecastCollectionViewAdapterDelegate: AnyObject {
  func weatherForecastCollectionViewAdapter(_ adapter: WeatherForecastCollectionViewAdapter,
                                            didTapItemAtIndex index: Int)
}

final class WeatherForecastCollectionViewAdapter: NSObject {
  
  // MARK: - Private Variables
  
  fileprivate let collectionView: ScalingCarouselView
  
  fileprivate var timePeriodInfo: [TimePeriodInfo] {
    didSet {
      collectionView.reloadData()
    }
  }
  
  // MARK: - Public Variables
  
  weak var delegate: WeatherForecastCollectionViewAdapterDelegate?
  
  // MARK: - Init Methods
  // MARK: -
  
  init(collectionView: ScalingCarouselView, timePeriodInfo: [TimePeriodInfo]) {
    self.collectionView = collectionView
    self.timePeriodInfo = timePeriodInfo
    
    super.init()
    collectionView.register(WeatherForecastCollectionViewCell.self)
    
    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.reloadData()
  }
}

// MARK: - UICollectionViewDataSource Methods Implementation
// MARK: -

extension WeatherForecastCollectionViewAdapter: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    return timePeriodInfo.count
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let collectionViewCell: WeatherForecastCollectionViewCell = collectionView.dequeReusableCell(forIndexPath: indexPath)
    collectionViewCell.configure(timePeriodInfo[indexPath.row])
    return collectionViewCell
  }
}

// MARK: - UICollectionViewDelegate Methods Implementation
// MARK: -

extension WeatherForecastCollectionViewAdapter: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    delegate?.weatherForecastCollectionViewAdapter(self, didTapItemAtIndex: indexPath.row)
  }
}
