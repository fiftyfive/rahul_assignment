//
//  WeatherForecastCarouselViewController.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/9/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import UIKit
import ScalingCarousel

final class WeatherForecastCarouselViewController: UIViewController {
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var collectionView: ScalingCarouselView!
  
  // MARK: - Public Variables
  
  var dailyWeatherData: [TimePeriodInfo]!
  
  // MARK: - Private Variables
  
  fileprivate var collectionViewAdapter: WeatherForecastCollectionViewAdapter?
  
  // MARK: - View Lifecycle Methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configCollectionViewAdapter()
  }
  
  // MARK: - Private Methods
  
  fileprivate func configCollectionViewAdapter() {
    collectionViewAdapter = WeatherForecastCollectionViewAdapter(collectionView: collectionView,
                                                                 timePeriodInfo: dailyWeatherData)
  }
}
