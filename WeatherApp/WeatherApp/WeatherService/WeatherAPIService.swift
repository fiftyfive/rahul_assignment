//
//  WeatherAPIService.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/8/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

final class WeatherAPIService {
  
  fileprivate struct Defaults {
    static let urlString: String = "https://api.darksky.net/forecast/0f1a64a5e10e523bee05913b79c9bc50"
  }
  
 static let shared = WeatherAPIService()
  let apiClient: APIClient
  
  init() {
    let url = URL.init(string: Defaults.urlString)
    if let url = url {
      apiClient = APIClient(baseURL: url)
    }
    else {
      print("API Client not initialized")
      fatalError()
    }
  }
  
  func fetchWeatherDetailedInfo(latitude: Double,
                                longitude: Double,
                                completion: @escaping (_ success: WeatherInfo?, _ error: Error?) -> Void) {
    let paramsInUrl = String(latitude) + ", " + String(longitude)
    
    apiClient.get(responseType: WeatherInfo.self,
                  urlPath: paramsInUrl,
                  parameters: nil,
                  isCachingResponse: true)
    { (response, urlResponse, error) in
                    if let error = response?.error {
                     let error =  NSError(domain: "sky.darknet",
                              code: response?.errorCode ?? 9999,
                              userInfo: [NSLocalizedDescriptionKey: error]) as Error
                      completion(nil, error)
                    }
                    else {
                      completion(response, error)
                    }
      
    }
  }
}
