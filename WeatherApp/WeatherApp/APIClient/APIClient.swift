//
//  APIClient.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/6/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

// MARK: - Utils

struct Utils {
  
  struct Constants {
    static let errorDomain = "errorDomain"
    static let unknownErrorCode = 9999999999
    static let unknownErrorMessage = "Unknown Error Message"
  }
  
  static func unknownError() -> Error {
    let unknownError: Error = NSError(domain: Constants.errorDomain,
                                      code: Constants.unknownErrorCode,
                                      userInfo: [NSLocalizedDescriptionKey: Constants.unknownErrorMessage]) as Error
    return unknownError
  }
}

// MARK: - Enum

enum RestMethod: String {
  case get = "GET"
  case post = "POST"
  case put = "PUT"
  case delete = "DELETE"
}

// MARK: - APIProtocol

protocol APIClientProtocol {
  var baseURL: URL { get }
  var session: URLSession { get }
  var defaultHeaders: [String: String] { get }

  func get<D: Decodable>(responseType: D.Type,
                         urlPath: String,
                         parameters: [String : String]?,
                         isCachingResponse: Bool,
                         completion: @escaping (D?, URLResponse?, Error?) -> Void)
  
  func post<E: Encodable, D: Decodable>(responseType: D.Type,
                                        urlPath: String,
                                        parameters: [String : String]?,
                                        body: E?,
                                        completion: @escaping (D?, URLResponse?, Error?) -> Void)
  func performRequest<D: Decodable>(responseType: D.Type,
                                    request: URLRequest,
                                    completion: @escaping (D?, URLResponse?, Error?) -> Void)
}

// MARK: - APIClient

class APIClient {
  let baseURL: URL
  let session: URLSession
  var defaultHeaders: [String : String]
  fileprivate var cachedResponse: [String: Decodable] = [:]
  
  init(baseURL: URL) {
    self.baseURL = baseURL
    self.session = URLSession(configuration: URLSessionConfiguration.default)
    self.defaultHeaders = ["Accept": "application/json"]
  }
  
  func configureDefaultHeader() -> [String: String] {
    // Here We can set auth key, api version info, platform specific
    return ["Accept": "application/json"]
  }
}

extension APIClient: APIClientProtocol {
  
  func get<D: Decodable>(responseType: D.Type,
                         urlPath: String,
                         parameters: [String : String]?,
                         isCachingResponse: Bool,
                         completion: @escaping (D?, URLResponse?, Error?) -> Void) {
    let url = baseURL.appendingPathComponent(urlPath).addParams(params: parameters)
    if let response = cachedResponse[url.absoluteString] as? D,
      isCachingResponse {
      completion(response, nil, nil)
      return
    }
    let request = buildRequest(url: url,
                               method: RestMethod.get.rawValue,
                               headers: defaultHeaders,
                               body: Optional<String>.none)
    performRequest(responseType: responseType,
                   request: request,
                   completion: completion)
  }
  
  func post<E: Encodable, D: Decodable>(responseType: D.Type,
                                        urlPath: String,
                                        parameters: [String : String]?,
                                        body: E?,
                                        completion: @escaping (D?, URLResponse?, Error?) -> Void) {
    let url = baseURL.appendingPathComponent(urlPath).addParams(params: parameters)
    let request = buildRequest(url: url,
                               method: RestMethod.post.rawValue,
                               headers: defaultHeaders,
                               body: body)
    performRequest(responseType: responseType,
                   request: request,
                   completion: completion)
  }

  
  func performRequest<D: Decodable>(responseType: D.Type,
                                    request: URLRequest,
                                    completion: @escaping (D?, URLResponse?, Error?) -> Void) {
    self.session.dataTask(with: request) { [weak self] (data, response, error) in
      guard let strongSelf = self else {
        return
      }
 
      if error != nil || data == nil {
        completion(nil, response, error)
      } else {
        guard let responseData = data else {
          completion(nil, response, error)
          return
        }
        guard error == nil else {
          completion(nil, response, error!)
          return
        }
        let decoder = JSONDecoder()
        do {
          let result = try decoder.decode(D.self, from: responseData)
          if let urlString = request.url?.absoluteString {
            strongSelf.cachedResponse[urlString] = result
          }
          completion(result, response, nil)
        } catch {
          completion(nil, response, error)
        }
      }
      }.resume()
  }
  
  private func buildRequest<E: Encodable>(url: URL,
                                          method: String,
                                          headers: [String: String]?,
                                          body: E?) -> URLRequest {
    var request = URLRequest(url: url)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = method
    if let requestHeaders = headers {
      for (key, value) in requestHeaders {
        request.addValue(value, forHTTPHeaderField: key)
      }
    }
    if let requestBody = body {
      let encoder = JSONEncoder();
      request.httpBody = try? encoder.encode(requestBody)
    }
    return request
  }
}

// MARK: - Extensions

fileprivate extension URL {
  func addParams(params: [String: String]?) -> URL {
    guard let params = params else {
      return self
    }
    var urlComp = URLComponents(url: self, resolvingAgainstBaseURL: true)!
    var queryItems = [URLQueryItem]()
    for (key, value) in params {
      queryItems.append(URLQueryItem(name: key, value: value))
    }
    urlComp.queryItems = queryItems
    return urlComp.url!
  }
}
