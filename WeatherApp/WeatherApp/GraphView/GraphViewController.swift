//
//  GraphViewController.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/8/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import UIKit
import Charts

class GraphViewController: UIViewController {
  
  // MARK: - Private Variables
  
  var hourlyWeatherData: [TimePeriodInfo]!
  
  // MARK: - IBOutlets

  @IBOutlet var chartView: LineChartView!
  
  // MARK: - View Lifecycle Methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configChartView()
  }
  
  // MARK: - Private Methods

  fileprivate func getHourForTimePeriod(_ timePeriod: TimePeriodInfo) ->  Double {
    let time = Date(timeIntervalSince1970: timePeriod.time)
    return Double(time.hour)
  }
  
  fileprivate func configChartView() {
    chartView.rightAxis.enabled = false
    hourlyWeatherData = hourlyWeatherData.sorted { $0.time < $1.time}
    let values = hourlyWeatherData.map { (timePeriod) -> ChartDataEntry in
      return ChartDataEntry(x: getHourForTimePeriod(timePeriod), y: timePeriod.temperature)
    }
    
    let set1 = LineChartDataSet(values: values, label: "DataSet 1")
    set1.drawIconsEnabled = false
    
    set1.lineDashLengths = [5, 2.5]
    set1.highlightLineDashLengths = [5, 2.5]
    set1.setColor(.black)
    set1.setCircleColor(.black)
    set1.lineWidth = 1
    set1.circleRadius = 3
    set1.drawCircleHoleEnabled = false
    set1.valueFont = .systemFont(ofSize: 9)
    set1.formLineDashLengths = [5, 2.5]
    set1.formLineWidth = 1
    set1.formSize = 15
    
    let gradientColors = [ChartColorTemplates.colorFromString("#00ff0000").cgColor,
                          ChartColorTemplates.colorFromString("#ffff0000").cgColor]
    let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
    
    set1.fillAlpha = 1
    set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
    set1.drawFilledEnabled = true
    
    let data = LineChartData(dataSet: set1)
    chartView.data = data
    
  }
}
