//
//  UIViewControllerExtensions.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/8/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import UIKit

extension UIViewController {
  static func containerStoryboardName() -> String {
  // Default is main
    return "Main"
  }
  
  static func initFromNib() -> UIViewController {
    return UIStoryboard.init(name: containerStoryboardName(), bundle: nil).instantiateViewController(withIdentifier: String(describing: self))
  }
}
