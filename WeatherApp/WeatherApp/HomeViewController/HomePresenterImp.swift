//
//  HomePresenterImp.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/9/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

final class HomePresenterImpl {
  
  // MARK: - Public properties
  
  var interactor: HomeInteractorInput!
  
  // MARK: - Private properties
  
  fileprivate weak var view: HomeView?
  fileprivate let router: HomeRouter
  fileprivate var weatherInfo: WeatherInfo?
  fileprivate var latitudeTextFieldValue: Double?
  fileprivate var longitudeTextFieldValue: Double?
  
  // MARK: - Initialization Method Implementation
  
  init(view: HomeView, router: HomeRouter) {
    self.view = view
    self.router = router
  }
}

extension HomePresenterImpl: HomePresenter {
  
  func resetLongitudeValue() {
    longitudeTextFieldValue = nil
  }
  
  func resetLatitudeValue() {
    latitudeTextFieldValue = nil
  }
  
  func latitudeTextFieldUpdated(value: Double) {
    latitudeTextFieldValue = value
  }
  
  func longitudeTextFieldUpdated(value: Double) {
    longitudeTextFieldValue = value
  }
  
  func getWeatherTapped() {
    
    guard let latValue = latitudeTextFieldValue,
      let longValue = longitudeTextFieldValue else {
        view?.showAlert(message: "Please enter value")
        return
    }
    view?.showHud(message: "Fetching Weather Info")
    interactor.fetchWeatherInfo(latitude: latValue, longitude: longValue)
  }
}

extension HomePresenterImpl: HomeInteractorOutput {
  
  func handleFetchWeatherInfoSuccess(response: WeatherInfo) {
    view?.removeHud()
    weatherInfo = response
    if let dataSource = response.hourly.data {
     // router.pushWeatherForecastScreen(datasource: dataSource)
      router.pushTemperatureGraphScreen(datasource: dataSource)
    }
  }
  
  func handleFetchWeatherInfoError(error: Error) {
    view?.removeHud()
    view?.showAlert(message: error.localizedDescription)
  }
}
