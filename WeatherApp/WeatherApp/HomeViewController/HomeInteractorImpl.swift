//
//  HomeInteractorImpl.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/9/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

class HomeInteractorImpl {
  
  // MARK: - Fileprivate Variables
  
  fileprivate weak var output: HomeInteractorOutput?
  fileprivate let service: WeatherAPIService
  
  // MARK: - Initialization Methods Implementaiton
  
  init(output: HomeInteractorOutput,
       service: WeatherAPIService) {
    
    self.output = output
    self.service = service
  }
}

// MARK: - HomeInteractorInput Implementation

extension HomeInteractorImpl: HomeInteractorInput {
  
  func fetchWeatherInfo(latitude: Double, longitude: Double) {
    
    service.fetchWeatherDetailedInfo(latitude: latitude,
                                     longitude: longitude) { [weak self] (response, error) in
      guard let strongSelf = self else {
        return
      }
      if let response = response {
        strongSelf.output?.handleFetchWeatherInfoSuccess(response: response)
      }
      else if let error = error {
        strongSelf.output?.handleFetchWeatherInfoError(error: error)
      }
      else {
        strongSelf.output?.handleFetchWeatherInfoError(error: Utils.unknownError())
      }
    }
  }
}
