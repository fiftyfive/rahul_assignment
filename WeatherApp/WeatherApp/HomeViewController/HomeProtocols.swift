//
//  HomeProtocols.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/9/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

protocol HomePresenter {
  func resetLatitudeValue()
  func resetLongitudeValue()
  func latitudeTextFieldUpdated(value: Double)
  func longitudeTextFieldUpdated(value: Double)
  func getWeatherTapped()
}

protocol HomeView: AnyObject {
  func showAlert(message: String)
  func showHud(message: String?)
  func removeHud()
}

protocol HomeInteractorInput {
  func fetchWeatherInfo(latitude: Double, longitude: Double)
}

protocol HomeInteractorOutput: AnyObject {
  func handleFetchWeatherInfoSuccess(response: WeatherInfo)
  func handleFetchWeatherInfoError(error: Error)
}

protocol HomeRouter {
  func pushWeatherForecastScreen(datasource: [TimePeriodInfo])
  func pushTemperatureGraphScreen(datasource: [TimePeriodInfo])
}
