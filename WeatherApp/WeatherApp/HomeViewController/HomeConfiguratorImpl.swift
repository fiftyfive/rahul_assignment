//
//  HomeConfiguratorImpl.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/9/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

class HomeConfiguratorImpl {
  
  // MARK: - Public Methods Implementation
  
  func configure(viewController: HomeViewController) {
    
    let router = HomeRouterImpl(viewController: viewController)
    let presenter = HomePresenterImpl(view: viewController, router: router)
    
    let apiService = WeatherAPIService.shared
    let interactor = HomeInteractorImpl(output: presenter, service: apiService)
    presenter.interactor = interactor
    viewController.presenter = presenter
  }
}
