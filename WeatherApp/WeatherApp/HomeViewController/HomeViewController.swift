//
//  ViewController.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/4/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeViewController: UIViewController {

  fileprivate struct Defaults {
    static let latitudeTag: Int = 56
    static let longitudeTag: Int = 57
  }
  
  // MARK: - Public Variables
  
  var presenter: HomePresenter!

  // MARK: - IBOutlets

  @IBOutlet weak var latitudeTextField: UITextField! {
    didSet {
      latitudeTextField.tag = Defaults.latitudeTag
    }
  }
  
  @IBOutlet weak var longitudeTextField: UITextField! {
    didSet {
      longitudeTextField.tag = Defaults.longitudeTag
    }
  }
  
  fileprivate var configurator: HomeConfiguratorImpl = HomeConfiguratorImpl()
  // MARK: - View Lifecycle Methods

  override func viewDidLoad() {
    super.viewDidLoad()
    configurator.configure(viewController: self)
    // Do any additional setup after loading the view, typically from a nib.
    
  }
  
  // MARK: - IBAction Methods

  @IBAction func getWeatherButtonTapped() {
    presenter.getWeatherTapped()
  }
}

// MARK: - HomeView Implementation

extension HomeViewController: HomeView {
  
  func showAlert(message: String) {
    DispatchQueue.main.async {
      let alertController = UIAlertController(title: nil,
                                              message: message,
                                              preferredStyle: .alert)
      alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
      self.present(alertController, animated: false, completion: nil)
    }
  }
  
  func showHud(message: String?) {
    SVProgressHUD.show(withStatus: message)
  }
  
  func removeHud() {
    SVProgressHUD.dismiss()
  }
}

// MARK: - UITextFieldDelegate Implementation

extension HomeViewController: UITextFieldDelegate {
  
  func textField(_ textField: UITextField,
                 shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {
   
    let textFieldText: NSString = (textField.text ?? "") as NSString
    let textAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
  
    switch textField.tag {
    case Defaults.latitudeTag:
      guard let latValue = Double(textAfterUpdate) else {
        presenter.resetLatitudeValue()
        return true
      }
      presenter.latitudeTextFieldUpdated(value: latValue)
    case Defaults.longitudeTag:
      guard let longValue = Double(textAfterUpdate) else {
        presenter.resetLongitudeValue()
        return true
      }
      presenter.longitudeTextFieldUpdated(value: longValue)
    default:
       return true
    }
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
