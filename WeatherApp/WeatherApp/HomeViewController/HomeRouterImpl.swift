//
//  HomeRouterImpl.swift
//  WeatherApp
//
//  Created by Rahul Khandal on 2/9/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation

final class HomeRouterImpl: HomeRouter {
  
  // MARK: - Private Weak Properties
  
  fileprivate weak var viewController: HomeViewController?
  
  // MARK: - Initialiaztion Methods Implementation
  
  init(viewController: HomeViewController) {
    self.viewController = viewController
  }
  
  // MARK: - HomeRouter Implementation
  
  func pushWeatherForecastScreen(datasource: [TimePeriodInfo]) {
    
    guard let weatherForecastVC = WeatherForecastCarouselViewController.initFromNib() as? WeatherForecastCarouselViewController else {
      return
    }
    weatherForecastVC.dailyWeatherData = datasource
     DispatchQueue.main.async {
     self.viewController?.navigationController?.pushViewController(weatherForecastVC, animated: true)
    }
  }
  
  func pushTemperatureGraphScreen(datasource: [TimePeriodInfo]) {
    guard let graphVC = GraphViewController.initFromNib() as? GraphViewController else {
      return
    }
    graphVC.hourlyWeatherData = datasource
    DispatchQueue.main.async {
      self.viewController?.navigationController?.pushViewController(graphVC, animated: true)
    }
  }
}
