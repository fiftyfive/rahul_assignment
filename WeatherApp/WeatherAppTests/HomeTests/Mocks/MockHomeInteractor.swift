//
//  MockHomeInteractor.swift
//  WeatherAppTests
//
//  Created by Rahul Khandal on 2/10/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation
@testable import WeatherApp

final class MockHomeInteractor: HomeInteractorInput {
  weak var output: HomeInteractorOutput?

  var fetchWeatherInfoCalled: Bool = false
  var shouldFetchWeatherInfoReturnResponse: Bool = false
  var response: WeatherInfo?
  
  var latitudeValueForFetch: Double?
  var longitudeValueForFetch: Double?

  func fetchWeatherInfo(latitude: Double, longitude: Double) {
    latitudeValueForFetch = latitude
    longitudeValueForFetch = longitude
    fetchWeatherInfoCalled = true
    
    if let response = response,
      shouldFetchWeatherInfoReturnResponse {
      output?.handleFetchWeatherInfoSuccess(response: response)
    }
    else {
      output?.handleFetchWeatherInfoError(error: Utils.unknownError())
    }
  }
}
