//
//  MockHomeViewController.swift
//  WeatherAppTests
//
//  Created by Rahul Khandal on 2/10/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation
@testable import WeatherApp

final class MockHomeViewController: HomeView {
  
  var doesShowAlertCalled = false
  
  func showAlert(message: String) {
    doesShowAlertCalled = true
  }
}
