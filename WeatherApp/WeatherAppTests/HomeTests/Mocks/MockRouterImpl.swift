//
//  MockRouterImpl.swift
//  WeatherAppTests
//
//  Created by Rahul Khandal on 2/10/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import Foundation
@testable import WeatherApp

final class MockRouterImp: HomeRouter {
  
  var doesWeatherForecastScreenPushed = false
  var doesTemperatureGraphScreenPushed = false
  
  func pushWeatherForecastScreen(datasource: [TimePeriodInfo]) {
    doesWeatherForecastScreenPushed = true
  }
  
  func pushTemperatureGraphScreen(datasource: [TimePeriodInfo]) {
    doesTemperatureGraphScreenPushed = true
  }
}
