//
//  HomeTest.swift
//  WeatherAppTests
//
//  Created by Rahul Khandal on 2/10/19.
//  Copyright © 2019 Rahul Khandal. All rights reserved.
//

import XCTest
@testable import WeatherApp

class HomePresenterTests: XCTestCase {
  
  
  var mockInteractor: MockHomeInteractor!
  var mockViewController: MockHomeViewController!
  var mockRouter: MockRouterImp!
  
  var sut: HomePresenterImpl!
 
  override func setUp() {
    super.setUp()
    configureSUT()
  
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testResetLatitudeValue() {
    sut.resetLatitudeValue()
    sut.getWeatherTapped()
    XCTAssertTrue(mockInteractor.fetchWeatherInfoCalled == false, "As latitude doesnot have value interactor is not called")
    XCTAssertTrue(mockViewController.doesShowAlertCalled == true, "As latitude missing alert is shown")
  }
  
  func testResetLongitudeValue() {
    sut.resetLongitudeValue()
    sut.getWeatherTapped()
    XCTAssertTrue(mockInteractor.fetchWeatherInfoCalled == false, "As longitude doesnot have value interactor is not called")
    XCTAssertTrue(mockViewController.doesShowAlertCalled == true, "As longitude missing alert is shown")
  }
  
  func testLatitudeTextFieldUpdated() {
    let latValue = Double(23)
    let longValue = Double(32)
    sut.latitudeTextFieldUpdated(value: latValue)
    sut.longitudeTextFieldUpdated(value: longValue)
    sut.getWeatherTapped()
    XCTAssertTrue(mockInteractor.latitudeValueForFetch == latValue)
  }
  
  func testLongitudeTextFieldUpdated() {
    let latValue = Double(23)
    let longValue = Double(32)
    sut.latitudeTextFieldUpdated(value: latValue)
    sut.longitudeTextFieldUpdated(value: longValue)
    sut.getWeatherTapped()
    XCTAssertTrue(mockInteractor.longitudeValueForFetch == longValue)
  }
  
  func testGetWeatherTapped() {
    let latValue = Double(23)
    let longValue = Double(32)
    sut.latitudeTextFieldUpdated(value: latValue)
    sut.longitudeTextFieldUpdated(value: longValue)
    mockInteractor.fetchWeatherInfoCalled = false
    
    sut.getWeatherTapped()
    XCTAssertTrue(mockInteractor.fetchWeatherInfoCalled)
    XCTAssertTrue(mockInteractor.longitudeValueForFetch == longValue)
    XCTAssertTrue(mockInteractor.latitudeValueForFetch == latValue)
  
    mockInteractor.fetchWeatherInfoCalled = false
    sut.resetLongitudeValue()
    sut.getWeatherTapped()
    XCTAssertTrue(mockInteractor.fetchWeatherInfoCalled == false)

  }
  
  func testHandleFetchWeatherInfoSuccess() {
    guard let weatherInfo = getJSONFromStubbedFile() else {
      return
    }
    sut.latitudeTextFieldUpdated(value: weatherInfo.latitude)
    sut.longitudeTextFieldUpdated(value: weatherInfo.longitude)
    mockInteractor.response = weatherInfo
    mockInteractor.shouldFetchWeatherInfoReturnResponse = true
    
    sut.getWeatherTapped()
    XCTAssertTrue(mockRouter.doesTemperatureGraphScreenPushed)
  }
  
  func testHandleFetchWeatherInfoError() {
    guard let weatherInfo = getJSONFromStubbedFile() else {
      return
    }
    sut.latitudeTextFieldUpdated(value: weatherInfo.latitude)
    sut.longitudeTextFieldUpdated(value: weatherInfo.longitude)
    mockInteractor.shouldFetchWeatherInfoReturnResponse = false
    
    sut.getWeatherTapped()
    XCTAssertTrue(mockRouter.doesTemperatureGraphScreenPushed == false)
    XCTAssertTrue(mockViewController.doesShowAlertCalled)
  }
  
  fileprivate func configureSUT() {
    self.mockInteractor = MockHomeInteractor()
    self.mockRouter = MockRouterImp()
    self.mockViewController = MockHomeViewController()
    
    self.sut = HomePresenterImpl(view: self.mockViewController,
                                 router: self.mockRouter)
    self.sut.interactor = mockInteractor
    mockInteractor.output = sut
  }
  
  fileprivate func getJSONFromStubbedFile() -> WeatherInfo? {
    let decoder = JSONDecoder()

    if let path = Bundle(for: type(of: self)).path(forResource: "Weather", ofType: "json") {
      do {
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
         let result = try decoder.decode(WeatherInfo.self, from: data)
        
        return result
      }
      catch let error {
        print(error.localizedDescription)
      }
    }
    return nil
  }

}
